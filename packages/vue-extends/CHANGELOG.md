# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [](https://github.com/viarotel-org/packages/compare/v0.7.2...v0.7.3) (2023-09-06)

**Note:** Version bump only for package @viarotel-org/vue-extends





# [](https://github.com/viarotel-org/packages/compare/v0.7.1...v0.7.2) (2023-09-06)

**Note:** Version bump only for package @viarotel-org/vue-extends





# [](https://github.com/viarotel-org/packages/compare/v0.7.0...v0.7.1) (2023-09-06)

**Note:** Version bump only for package @viarotel-org/vue-extends





# [](https://github.com/viarotel-org/packages/compare/v0.6.1...v0.7.0) (2023-09-05)

**Note:** Version bump only for package @viarotel-org/vue-extends





# [](https://github.com/viarotel-org/packages/compare/v0.6.0...v0.6.1) (2023-09-05)

**Note:** Version bump only for package @viarotel-org/vue-extends





# [](https://github.com/viarotel-org/packages/compare/v0.5.11...v0.6.0) (2023-09-05)

**Note:** Version bump only for package @viarotel-org/vue-extends






## [0.5.11](https://github.com/viarotel-org/packages/compare/v0.5.10...v0.5.11) (2023-08-17)


### Performance Improvements

* 🔧 vue-extends 演示页面优化并去除无用依赖 ([56d16c2](https://github.com/viarotel-org/packages/commit/56d16c225b72c92905ed29b763d96b37f812bc81))






## [0.5.9](https://github.com/viarotel-org/packages/compare/v0.5.8...v0.5.9) (2023-08-14)

**Note:** Version bump only for package @viarotel-org/vue-extends
